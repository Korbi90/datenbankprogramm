package common;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;

public class CreatePDF {

    // Config
    private static final String TXT_STATEMENT = "STATEMENT";
    private static final String TXT_FEE_SCHEDULE = "FEE SCHEDULE";
    private static final int MARGIN_LEFT = 45;
    private static final int MARGIN_LEFT_CASH_LIST = 405;
    private static final int Y_POS_HEADER = 780;
    private static final int Y_POS_CONTENT = 755;
    private static final int Y_POS_CONTENT_FEE_SCHEDULE = 555;
    private static final int Y_POS_FOOTER = 60;
    private static final int LINE_LENGTH = 518;
    private static final int LINE_LENGTH_CASH_LIST = 158;

    private static final int ROWS_PER_PAGE = 57;
    private static final int NEEDED_ROWS_FOR_SHARE_SUMMARY = 12;
    private static final int NEEDED_ROWS_FOR_CASH_SUMMARY = 5;
    private static final int SUMMARY_LINE_HEIGHT = 15;
    private static final int LINE_HEIGHT = 12;
    private static final int FONT_SIZE = 9;
    private int pageCurrent = 1;
    private int pageTotal = 1;

    private boolean periodic = false;

    private DepotCalc depCal;
    private final Database userDB;

    //Constructor sets the fee rate in percent
    public CreatePDF(Database userDBReference) {
        this.userDB = userDBReference;
        this.depCal = new DepotCalc(20);
    }

    public void statement(Date dateStart, Date dateEnd, String depot) throws IOException {

        // Create an ArrayList of shares
        ArrayList<String> shareList = new ArrayList<>();

        // Create an ArrayList of cash
        ArrayList<String> cashList = new ArrayList<>();

        //Create DepotCalc Object with 20% fee
        //DepotCalc depCalc = new DepotCalc(20);
        // Get shares from database
        ResultSet shareData;
        ResultSet cashData;
        if ((dateStart == null) || (dateEnd == null)) {
            shareData = userDB.getSortedDepotFromServer(depot, false);
            cashData = userDB.getSortedDepotFromServer(depot, true);
            periodic = false;
        } else {
            shareData = userDB.getSortedDepotOverPeriod(depot, dateStart, dateEnd, false);
            cashData = userDB.getSortedDepotOverPeriod(depot, dateStart, dateEnd, true);
            periodic = true;
        }

        // Bring the data in the right format
        int amountShares = _getFormatedShareList(shareData, shareList);
        int amountCash = _getFormatedCashList(cashData, cashList);
        userDB.disconnect();

        // 1 Page per List = 2 
        // 
        pageTotal = 2 + amountShares / ROWS_PER_PAGE + amountCash / ROWS_PER_PAGE;
        boolean extraPageForShareSummary = false;
        // If more than 47 shares are on the last share page, 
        // add an extra page for share summary
        if ((amountShares % ROWS_PER_PAGE) > (ROWS_PER_PAGE - NEEDED_ROWS_FOR_SHARE_SUMMARY)) {
            pageTotal++;
            extraPageForShareSummary = true;
        } else if (amountShares % ROWS_PER_PAGE == 0) {
            //extraPageForShareSummary = true;
            //pageTotal++;
        }
        boolean extraPageForCashSummary = false;
        // If more than x shares are on the last share page, 
        // add an extra page for share summary
        if ((amountCash % ROWS_PER_PAGE) > (ROWS_PER_PAGE - NEEDED_ROWS_FOR_CASH_SUMMARY)) {
            pageTotal++;
            extraPageForCashSummary = true;
        } else if (amountCash % ROWS_PER_PAGE == 0) {
            //extraPageForCashSummary = true;
            //pageTotal++;
        }
        System.out.println(amountShares + " shares in list from depot " + depot);
        System.out.println(amountCash + " cash/dividend entries in list from depot " + depot);

        // PDF File name (should be in the config)
        String filename = "statement.pdf";

        PDDocument doc;
        PDPage page;

        try {
            // Create PDF doc
            doc = new PDDocument();

            // create and add first Page
            page = new PDPage(PDPage.PAGE_SIZE_A4);
            doc.addPage(page);

            // Create content stream
            PDPageContentStream content = new PDPageContentStream(doc, page);

            // Create a new font object by loading a TrueType font into the document
            PDFont fontMplus = PDTrueTypeFont.loadTTF(doc, "ttf/mplus-1m-regular.ttf");
            PDFont fontMplusBold = PDTrueTypeFont.loadTTF(doc, "ttf/mplus-1m-bold.ttf");
            
            // Fill content stream with data
            _header(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, dateStart, dateEnd);

            // Begin of share list output in pdf stream
            int yPos;
            int i = 0;
            if (amountShares > 0) {
                header_share_list(content, MARGIN_LEFT, Y_POS_CONTENT, fontMplusBold);
                for (String row : shareList) {
                    i++;
                    content.beginText();
                    content.setFont(fontMplus, FONT_SIZE);
                    yPos = Y_POS_CONTENT - i * LINE_HEIGHT;
                    content.moveTextPositionByAmount(MARGIN_LEFT, yPos);
                    content.drawString(row);
                    content.endText();
                    content.setLineWidth(0.1f);
                    content.drawLine(MARGIN_LEFT, yPos - 3, MARGIN_LEFT + LINE_LENGTH, yPos - 3);
                    if (i >= ROWS_PER_PAGE) {
                        i = 0;
                        _footer(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, pageCurrent, pageTotal);
                        content.close();
                        // create and add new Page
                        page = new PDPage(PDPage.PAGE_SIZE_A4);
                        doc.addPage(page);
                        pageCurrent++;
                        // Create content stream
                        content = new PDPageContentStream(doc, page);
                        _header(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, dateStart, dateEnd);
                        if (amountShares % ROWS_PER_PAGE != 0) {
                            header_share_list(content, MARGIN_LEFT, Y_POS_CONTENT, fontMplusBold);
                        }
                    }
                }

                if (extraPageForShareSummary) {
                    i = 0;
                    _footer(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, pageCurrent, pageTotal);
                    content.close();
                    // create and add new Page
                    page = new PDPage(PDPage.PAGE_SIZE_A4);
                    doc.addPage(page);
                    pageCurrent++;
                    // Create content stream
                    content = new PDPageContentStream(doc, page);
                    _header(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, dateStart, dateEnd);
                    yPos = Y_POS_CONTENT;
                } else {
                    yPos = Y_POS_CONTENT - i * LINE_HEIGHT - 20;
                }
                _stmtShareSummary(content,
                        periodic,
                        MARGIN_LEFT + 298,
                        yPos - 20,
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getCosts()), 20),
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getSellCosts()), 20),
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getBuyCosts()), 20),
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getProceeds()), 20),
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getGrossProfit()), 20),
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getBookValue()), 20),
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getFee()), 20),
                        fontMplus,
                        fontMplusBold);
            } else {
                content.beginText();
                content.moveTextPositionByAmount(MARGIN_LEFT, Y_POS_CONTENT - 20);
                content.setFont(fontMplusBold, 10);
                content.drawString("NO SHARES IN DEPOT!");
                content.endText();
            }
            _footer(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, pageCurrent, pageTotal);

            // close content stream
            content.close();

            // create and add Page
            page = new PDPage(PDPage.PAGE_SIZE_A4);
            doc.addPage(page);
            pageCurrent++;

            // Create content stream
            content = new PDPageContentStream(doc, page);

            // Fill content stream with data
            _header(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, dateStart, dateEnd);

            // Begin of cash list output in pdf stream
            i = 0;
            if (amountCash > 0) {
                header_cash_list(content, fontMplusBold);
                for (String row : cashList) {
                    i++;
                    content.beginText();
                    content.setFont(fontMplus, FONT_SIZE);
                    yPos = Y_POS_CONTENT - i * LINE_HEIGHT;
                    content.moveTextPositionByAmount(MARGIN_LEFT_CASH_LIST, yPos);
                    content.drawString(row);
                    content.endText();
                    content.setLineWidth(0.1f);
                    content.drawLine(MARGIN_LEFT_CASH_LIST, yPos - 3, MARGIN_LEFT_CASH_LIST + LINE_LENGTH_CASH_LIST, yPos - 3);
                    if (i >= ROWS_PER_PAGE) {
                        i = 0;
                        _footer(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, pageCurrent, pageTotal);
                        content.close();
                        // create and add new Page
                        page = new PDPage(PDPage.PAGE_SIZE_A4);
                        doc.addPage(page);
                        pageCurrent++;
                        // Create content stream
                        content = new PDPageContentStream(doc, page);
                        _header(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, dateStart, dateEnd);
                        if (amountCash % ROWS_PER_PAGE != 0) {
                            header_cash_list(content, fontMplusBold);
                        }
                    }
                }

                if (extraPageForCashSummary) {
                    i = 0;
                    _footer(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, pageCurrent, pageTotal);
                    content.close();
                    // create and add new Page
                    page = new PDPage(PDPage.PAGE_SIZE_A4);
                    doc.addPage(page);
                    pageCurrent++;
                    // Create content stream
                    content = new PDPageContentStream(doc, page);
                    _header(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, dateStart, dateEnd);
                    yPos = Y_POS_CONTENT;
                } else {
                    yPos = Y_POS_CONTENT - i * LINE_HEIGHT - 20;
                }
                _stmtCashSummary(content,
                        MARGIN_LEFT + 298,
                        yPos - 20,
                        _fixedStringLengthAlignRight(String.format("%.2f", depCal.getCash()), 20),
                        fontMplus,
                        fontMplusBold);
            } else {
                content.beginText();
                content.moveTextPositionByAmount(MARGIN_LEFT, Y_POS_CONTENT - 20);
                content.setFont(fontMplusBold, 10);
                content.drawString("NO CASH OR DIVIDEND IN DEPOT!");
                content.endText();
            }
            _footer(content, fontMplus, fontMplusBold, TXT_STATEMENT, depot, pageCurrent, pageTotal);

            // close content stream
            content.close();

            // save file
            doc.save(filename);

            // close doc
            doc.close();

            pageCurrent = 1;
        } catch (Exception e) {
            System.out.println(e);
        }

        // Statement in java öffnen
        File statement = new java.io.File(filename);
        Desktop dt = Desktop.getDesktop();
        try {
            // open statement with external app
            dt.open(statement);
            // print statement (not supported by linux mint)
            //dt.print(statement);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    /**
     *
     * @param dateStart begin of the period
     * @param dateEnd end of the period
     * @param depot
     * @throws IOException
     */
    public void feeSchedule(Date dateStart,
            Date dateEnd, String depot) throws IOException {

        // Create an ArrayList of shares
        ArrayList<String> shareList = new ArrayList<>();

        ResultSet depotData;
        // Get shares from database
        // They are sorted, because this function already existed
        depotData = userDB.getSortedDepotOverPeriod(depot, dateStart, dateEnd, false);

        // Bring the data in the right format
        // The name of this function does not cover its whole potential:
        // It formats the shares data and sums up costs, proceeds, gross profit
        // and book value
        int amountShares = _getFormatedShareList(depotData, shareList);
        userDB.disconnect();

        pageTotal = 1;
        System.out.println(amountShares + " shares in list");

        // PDF File name (should be in the config)
        String filename = "fee_schedule.pdf";

        PDDocument doc;
        PDPage page;

        try {
            // Create PDF doc
            doc = new PDDocument();

            // create and add first Page
            page = new PDPage(PDPage.PAGE_SIZE_A4);
            doc.addPage(page);

            // Create content stream
            PDPageContentStream content = new PDPageContentStream(doc, page);

            // Create a new font object by loading a TrueType font into the document
            PDFont fontMplus = PDTrueTypeFont.loadTTF(doc, "ttf/mplus-1m-regular.ttf");
            PDFont fontMplusBold = PDTrueTypeFont.loadTTF(doc, "ttf/mplus-1m-bold.ttf");
            
            // Fill content stream with data
            _header(content, fontMplus, fontMplusBold, TXT_FEE_SCHEDULE, depot, dateStart, dateEnd);


            _feeScheduleContent(content,
                    _fixedStringLengthAlignRight(String.format("%.2f", depCal.getSellCosts()), 20),
                    _fixedStringLengthAlignRight(String.format("%.2f", depCal.getProceeds()), 20),
                    _fixedStringLengthAlignRight(String.format("%.2f", depCal.getGrossProfit()), 20),
                    _fixedStringLengthAlignRight(String.format("%.2f", depCal.getBookValue()), 20),
                    _fixedStringLengthAlignRight(String.format("%.2f", depCal.getFee()), 20), // Fee
                    _fixedStringLengthAlignRight(String.format("%.2f", depCal.getGrossProfitNet()), 20), // Netto Profit
                    fontMplus,
                    fontMplusBold);

            _footer(content, fontMplus, fontMplusBold, TXT_FEE_SCHEDULE, depot, pageCurrent, pageTotal);

            // close content stream
            content.close();

            // save file
            doc.save(filename);

            // close doc
            doc.close();

            pageCurrent = 1;
        } catch (Exception e) {
            System.out.println(e);
        }

        // Statement in java öffnen
        File statement = new java.io.File(filename);
        Desktop dt = Desktop.getDesktop();
        try {
            // open statement with external app
            dt.open(statement);
            // print statement (not supported by linux mint)
            //dt.print(statement);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private static void _header(PDPageContentStream content, PDFont fontHeader, PDFont fontHeaderBold, String type, String depotNo, Date dateStart, Date dateEnd) {

        try {

            content.beginText();
            content.moveTextPositionByAmount(MARGIN_LEFT, Y_POS_HEADER);
            content.setFont(fontHeaderBold, 12);
            content.drawString(type + " ");
            content.setFont(fontHeader, 10);
            content.drawString("IN POUND STERLING");
                content.moveTextPositionByAmount(190, 0);
            if (dateStart != null) {
                DateFormat df = new SimpleDateFormat("dd.MM.yy");
                content.drawString("PERIOD OF ACCOUNTING: ");
                content.drawString(df.format(dateStart) + " - " + df.format(dateEnd));
            }
            content.moveTextPositionByAmount(242, 0);
            content.setFont(fontHeaderBold, 10);
            content.drawString("DEPOT NO. ");
            content.drawString(depotNo);
            content.endText();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void header_share_list(PDPageContentStream content, int xPos, int yPos, PDFont fontMplusBold) {

        try {
            content.beginText();
            content.moveTextPositionByAmount(xPos, yPos);
            content.setFont(fontMplusBold, FONT_SIZE);
            content.drawString(_fixedStringLengthAlignLeft("shares", 13));
            content.drawString("buy-date  ");
            content.drawString(" number  ");
            content.drawString("       costs ");
            content.drawString("sell-date   ");
            content.drawString("number  ");
            content.drawString("    proceeds  ");
            content.drawString("gross-profit ");
            content.drawString("inventory ");
            content.drawString("   book value");
            content.endText();
            content.setLineWidth(0.5f);
            content.drawLine(xPos, yPos - 3, xPos + LINE_LENGTH, yPos - 3);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void header_cash_list(PDPageContentStream content, PDFont fontMplusBold) {

        try {
            content.beginText();
            content.setFont(fontMplusBold, FONT_SIZE);
            content.moveTextPositionByAmount(MARGIN_LEFT_CASH_LIST, Y_POS_CONTENT);
            content.drawString("name         date             value");
            content.endText();
            content.setLineWidth(0.1f);
            content.drawLine(MARGIN_LEFT_CASH_LIST, Y_POS_CONTENT - 3, MARGIN_LEFT_CASH_LIST + LINE_LENGTH_CASH_LIST, Y_POS_CONTENT - 3);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void _stmtShareSummary(PDPageContentStream content,
            Boolean showFee,
            int xPos,
            int yPos,
            String totalCosts,
            String totalCostsSell,
            String totalCostsBuy,
            String totalProceeds,
            String totalGrossProfit,
            String totalBookValue,
            String Fees,
            PDFont fontMplus,
            PDFont fontMplusBold) {

        try {

            content.beginText();
            content.setFont(fontMplus, 10);

            content.moveTextPositionByAmount(xPos, yPos);
            content.drawString("                 Total Costs:");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalCosts);

            content.moveTextPositionByAmount(-120, -SUMMARY_LINE_HEIGHT);
            content.drawString("  Total Costs Of Sold Shares:");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalCostsSell);

            content.moveTextPositionByAmount(-120, -SUMMARY_LINE_HEIGHT);
            content.drawString("Total Costs Of Bought Shares:");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalCostsBuy);

            content.moveTextPositionByAmount(-120, -SUMMARY_LINE_HEIGHT);
            content.drawString("              Total Proceeds:");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalProceeds);

            content.moveTextPositionByAmount(-120, -SUMMARY_LINE_HEIGHT);
            content.drawString("          Total Gross Profit:");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalGrossProfit);

            content.moveTextPositionByAmount(-120, -SUMMARY_LINE_HEIGHT);
            content.drawString("            Total Book Value:");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalBookValue);

            if (showFee) {
                content.setFont(fontMplusBold, 10);
                content.moveTextPositionByAmount(-120, -SUMMARY_LINE_HEIGHT);
                content.drawString("      Fees In Pound Sterling:");
                content.moveTextPositionByAmount(120, 0);
                content.drawString(Fees);
            }

            content.endText();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void _stmtCashSummary(PDPageContentStream content,
            int xPos,
            int yPos,
            String totalCash,
            PDFont fontMplus,
            PDFont fontMplusBold) {

        try {

            content.beginText();
            content.setFont(fontMplus, 10);

            content.moveTextPositionByAmount(xPos, yPos);
            content.drawString("            Total Cash:      ");
            content.moveTextPositionByAmount(120, 0);
            content.drawString(totalCash);

            content.endText();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void _feeScheduleContent(PDPageContentStream content,
            String totalCosts,
            String totalProceeds,
            String totalGrossProfit,
            String totalBookValue,
            String Fees,
            String totalProfitNet,
            PDFont fontMplus,
            PDFont fontMplusBold) {

        
        try {

            int distance = 190;
            int lineheight = 30;
            content.beginText();
            content.setFont(fontMplus, 12);

            content.moveTextPositionByAmount( MARGIN_LEFT + 90, Y_POS_CONTENT_FEE_SCHEDULE);
            content.drawString("Buy Price Of Sold Shares:");
            content.moveTextPositionByAmount(distance, 0);
            content.drawString(totalCosts);

            content.moveTextPositionByAmount(-distance, -lineheight);
            content.drawString("Proceeds:");
            content.moveTextPositionByAmount(distance, 0);
            content.drawString(totalProceeds);

            content.moveTextPositionByAmount(-distance, -lineheight);
            content.drawString("Gross Profit:");
            content.moveTextPositionByAmount(distance, 0);
            content.drawString(totalGrossProfit);

            content.moveTextPositionByAmount(-distance, -lineheight);
            content.setFont(fontMplusBold, 12);
            content.drawString("Fees In Pound Sterling:");
            content.moveTextPositionByAmount(distance, 0);
            content.drawString(Fees);

            content.moveTextPositionByAmount(-distance, -lineheight);
            content.setFont(fontMplus, 12);
            content.drawString("Profit Net:");
            content.moveTextPositionByAmount(distance, 0);
            content.drawString(totalProfitNet);

            //content.moveTextPositionByAmount(-distance, -lineheight);
            //content.drawString("Book Value:");
            //content.moveTextPositionByAmount(distance, 0);
            //content.drawString(totalBookValue);
            content.endText();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void _footer(PDPageContentStream content, PDFont fontHeader, PDFont fontHeaderBold, String type, String depotNo, int pageCurrent, int pageTotal) {

        try {

            //content.setLineWidth(0.5f);
            //content.drawLine(xPos, yPos, xPos + 500, yPos);
            content.beginText();
            content.setFont(fontHeader, 8);
            content.moveTextPositionByAmount(MARGIN_LEFT, Y_POS_FOOTER - 11);
            content.drawString(type);
            content.moveTextPositionByAmount(215, 0);
            content.drawString("DEPOT NO. " + depotNo);
            content.moveTextPositionByAmount(231, 0);
            content.setFont(fontHeaderBold, 8);
            String PageTxt = _fixedStringLengthAlignRight("PAGE " + pageCurrent + " OF " + pageTotal, 18);
            content.drawString(PageTxt);
            content.endText();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static String _fixedStringLengthAlignRight(String string, int targetLength) {
        if (string == null) {
            string = "";
        }
        while (string.length() < targetLength) {
            string = " " + string;
        }
        return string;
    }

    private static String _fixedStringLengthAlignLeft(String string, int targetLength) {
        if (string == null) {
            string = "";
        }
        while (string.length() < targetLength) {
            string = string + " ";
        }
        return string;
    }

    private int _getFormatedShareList(ResultSet sharesData, ArrayList<String> shareList) {
        int count = 0;
        try {
            while (sharesData.next()) {
                // Create an instance of SimpleDateFormat used for formatting 
                // the string representation of date (dd.mm.yy)
                DateFormat df = new SimpleDateFormat("dd.MM.yy");

                // Skip first entry, contains mysql ID
                // align left and length shouldn't be too big so no changes need
                String shareName = _fixedStringLengthAlignLeft(sharesData.getString("share"), 11);
                // formats buyDate from sql-format to statement format dd.mm.yy
                String buyDate = "       N/A";
                String sellDate = "       N/A";

                Date _buyDate = sharesData.getDate("buyDate");
                if (!sharesData.wasNull()) {
                    buyDate = _fixedStringLengthAlignRight(df.format(_buyDate), 10);
                }

                String numberBuy = _fixedStringLengthAlignRight(sharesData.getString("buyNr"), 9);
                String costs = _fixedStringLengthAlignRight(sharesData.getString("costs"), 14);

                Date _sellDate = sharesData.getDate("sellDate");
                if (!sharesData.wasNull()) {
                    sellDate = _fixedStringLengthAlignRight(df.format(_sellDate), 10);
                }

                String numberSell = sharesData.getString("sellNr");
                if (sharesData.wasNull()) {
                    numberSell = "0";
                }
                numberSell = _fixedStringLengthAlignRight(numberSell, 9);

                String proceeds = sharesData.getString("proceeds");
                if (sharesData.wasNull()) {
                    proceeds = "0.00";
                }
                proceeds = _fixedStringLengthAlignRight(proceeds, 14);

                String profit = sharesData.getString("profit");
                if (sharesData.wasNull()) {
                    profit = "0.00";
                }
                profit = _fixedStringLengthAlignRight(profit, 14);

                String inventory = sharesData.getString("inventory");
                if (sharesData.wasNull()) {
                    inventory = "0";
                }
                // If inventory equals "0" the share is allready sold
                if (inventory.equals("0")) {
                    depCal.addSellCosts(sharesData.getDouble("costs"));
                    // If inventory unequals "0" the share is not yet sold
                } else {
                    depCal.addBuyCosts(sharesData.getDouble("costs"));
                }
                inventory = _fixedStringLengthAlignRight(inventory, 10);

                String bookValue = sharesData.getString("bookValue");
                if (sharesData.wasNull()) {
                    bookValue = "0.00";
                }
                bookValue = _fixedStringLengthAlignRight(bookValue, 14);

                depCal.addCost(sharesData.getDouble("costs"));
                depCal.addProceeds(sharesData.getDouble("proceeds"));
                depCal.addGrossProfit(sharesData.getDouble("profit"));
                depCal.addBookValue(sharesData.getDouble("bookValue"));

                String share = shareName + buyDate + numberBuy + costs + sellDate + numberSell
                        + proceeds + profit + inventory + bookValue;

                System.out.println(share);
                shareList.add(share);
                ++count;
            }
        } catch (SQLException sqlex) {
            System.err.println("Error from CreatePDF._getShareList");
            System.err.println("SQLException: " + sqlex.getMessage());
            System.err.println("SQLState: " + sqlex.getSQLState());
            System.err.println("VendorError: " + sqlex.getErrorCode());
        }
        return count;
    }

    private int _getFormatedCashList(ResultSet cashData, ArrayList<String> cashList) {
        int count = 0;
        try {
            while (cashData.next()) {
                // Create an instance of SimpleDateFormat used for formatting 
                // the string representation of date (dd.mm.yy)
                DateFormat df = new SimpleDateFormat("dd.MM.yy");

                // Skip first entry, contains mysql ID
                // align left and length shouldn't be too big so no changes need
                String name = _fixedStringLengthAlignLeft(cashData.getString("share"), 11);
                // formats buyDate from sql-format to statement format dd.mm.yy
                String buyDate = "       N/A";

                Date _buyDate = cashData.getDate("buyDate");
                if (!cashData.wasNull()) {
                    buyDate = _fixedStringLengthAlignRight(df.format(_buyDate), 10);
                }

                String bookValue = cashData.getString("bookValue");
                if (cashData.wasNull()) {
                    bookValue = "0.00";
                }
                bookValue = _fixedStringLengthAlignRight(bookValue, 14);

                depCal.addCash(cashData.getDouble("bookValue"));

                String share = name + buyDate + bookValue;

                System.out.println(share);
                cashList.add(share);
                ++count;
            }
        } catch (SQLException sqlex) {
            System.err.println("Error from CreatePDF._getShareList");
            System.err.println("SQLException: " + sqlex.getMessage());
            System.err.println("SQLState: " + sqlex.getSQLState());
            System.err.println("VendorError: " + sqlex.getErrorCode());
        } finally {
            return count;
        }
    }
}
