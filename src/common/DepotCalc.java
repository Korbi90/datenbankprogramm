/*
 * This Class calculates the costs and fee for statement or fee shedule
 */
package common;

/**
 *
 * @author jona
 */
public class DepotCalc {

    private double totalCosts = 0.0;
    private double totalCash = 0.0;
    private double totalBuyCosts = 0.0;
    private double totalSellCosts = 0.0;
    private double totalProceeds = 0.0;
    private double totalGrossProfit = 0.0;
    private double totalBookValue = 0.0;
    private final double feeMultiplier;
    private final double nettoMultiplier;

    //Constructor sets the fee rate in percent
    public DepotCalc(double feeInPercent) {
        this.feeMultiplier = feeInPercent / 100;
        this.nettoMultiplier = 1.0 - feeMultiplier;
    }

    // reset all calculations / set all vars to zero
    void reset() {
        totalCosts = 0.0;
        totalCash = 0.0;
        totalBuyCosts = 0.0;
        totalSellCosts = 0.0;
        totalProceeds = 0.0;
        totalGrossProfit = 0.0;
        totalBookValue = 0.0;
    }

    // getter and setter
    // I think they don't need a comment
    void addCost(double costs) {
        totalCosts += costs;
    }

    void addCash(double value) {
        totalCash += value;
    }

    void addBuyCosts(double costs) {
        totalBuyCosts += costs;
    }

    void addSellCosts(double costs) {
        totalSellCosts += costs;
    }

    void addProceeds(double costs) {
        totalProceeds += costs;
    }

    void addGrossProfit(double costs) {
        totalGrossProfit += costs;
    }

    void addBookValue(double costs) {
        totalBookValue += costs;
    }

    double getCosts() {
        return totalCosts;
    }

    double getCash() {
        return totalCash;
    }

    double getBuyCosts() {
        return totalBuyCosts;
    }

    double getSellCosts() {
        return totalSellCosts;
    }

    double getProceeds() {
        return totalProceeds;
    }

    double getGrossProfit() {
        return totalGrossProfit;
    }

    double getBookValue() {
        return totalBookValue;
    }

    double getFee() {
        return totalGrossProfit * feeMultiplier;
    }

    double getGrossProfitNet() {
        return totalGrossProfit * nettoMultiplier;
    }
}
