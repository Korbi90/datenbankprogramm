/**
 * @ Logical Implemantation Classes for the management of userdatabase
 *
 *
 * @author Jona
 * @version 1.11
 * @date 18.3.2016
 */
package common;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author jona
 */
public class Database {

    private Connection conn = null;
    private String user = null;
    private boolean isAdmin;
    private String pw = null;
    private String db = null;
    private String url = "jdbc:mysql://localhost:3306/";
    private String params = "?zeroDateTimeBehavior=convertToNull";

    //Constructors
    public Database(String userName, String password) {
        this.isAdmin = false;
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
            System.out.println("Error: com.mysql.jdbc.Driver could not be loaded.");
        }
        this.user = userName;
        this.pw = password;
        this.db = "deponiedb";
    }

    // Getter Methods
    public Connection getConnection() {
        return conn;
    }

    public String getDataBase() {
        return db;
    }

    public String getUserName() {
        return user;
    }

    public String getPassword() {
        return pw;
    }

    public boolean isAdmin() {
        return this.isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    // Comppare Methods
    public Boolean comparePassword(String pw) {
        return pw.equals(this.pw);
    }

    public void connect() {
        try {
            conn = DriverManager.getConnection(url + db + params, user, pw);
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
            JOptionPane.showMessageDialog(null, "Database error: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    public void disconnect() {
        try {
            conn.close();
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }
    }

    public boolean tblExists(String table) {

// assume that conn is an already created JDBC connection (see previous examples)
        connect();
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = conn.createStatement();

            rs = stmt.executeQuery("SELECT * FROM  `deponiedb`.`" + table);

        } catch (SQLException ex) {
            // handle any errors
            //JOptionPane.showMessageDialog(null, "Database error: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            JOptionPane.showMessageDialog(null, "Depot '" + table + "' not found!", "Error", JOptionPane.ERROR_MESSAGE);
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
            return false;
        } finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore

                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
            disconnect();
        }
        return true;
    }

    public ResultSet getSortedDepotFromServer(String depot, boolean isCash) {
        int iIsCash = 0;
        if (isCash) {
            iIsCash = 1;
        }
        this.connect();
        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + this.db + ".`" + depot
                    + "` WHERE isCash=" + iIsCash
                    + " ORDER BY `" + depot + "`.`share` ASC");
            return rs;
            // Now do something with the ResultSet ....
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }

        return rs;
    }

    public ResultSet getSortedDepotOverPeriod(String depot, java.util.Date dateStart, java.util.Date dateEnd, boolean isCash) {
        int iIsCash = 0;
        if (isCash) {
            iIsCash = 1;
        }
        this.connect();
        Statement stmt;
        ResultSet rs = null;

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        // throws runtime exception if conversions fail
        System.out.println("Retrieving data over period: from "
                + ft.format(dateStart) + " to " + ft.format(dateEnd));

        try {
            stmt = conn.createStatement();
            // Retrieves only those shares, which are bought before the end
            // of the period and which are not sold or sold in the period!
            // Couldn't check this yet, but should be ok :)
            rs = stmt.executeQuery("SELECT * FROM " + this.db + ".`" + depot + "` "
                    + "WHERE ((`buyDate`>='" + ft.format(dateStart)
                    + "' AND `buyDate`<='" + ft.format(dateEnd) + "') OR  "
                    + "(`sellDate`<='" + ft.format(dateEnd) + "' AND `sellDate`>='"
                    + ft.format(dateStart) + "')) AND `isCash`=" + iIsCash
                    + " ORDER BY `" + depot + "`.`share` ASC");
            return rs;
            // Now do something with the ResultSet ....
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }
        return rs;
    }

    public ResultSet getTableRowByID(String depot, int RowID) {

        this.connect();
        PreparedStatement stmt;
        ResultSet rs = null;
        // so net oder doch
        String sql = "SELECT * FROM " + this.db + ".`" + depot + "` WHERE id=?";

        try {
            stmt = conn.prepareStatement(sql);
            // get the sql table ID
            stmt.setInt(1, RowID);
            rs = stmt.executeQuery();
            return rs;
            // Now do something with the ResultSet ....
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("Fehler aus getTableRowByID()");
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }
        return rs;

    }

    // Method used for Admindepots
    public ResultSet getTableRowsByShareName(String depot, String sharename) {

        this.connect();
        PreparedStatement stmt;
        ResultSet rs = null;
        // so net oder doch
        String sql = "SELECT inventory FROM " + this.db + ".`" + depot + "` WHERE share=?";

        try {
            stmt = conn.prepareStatement(sql);
            // get the sql table ID
            stmt.setString(1, sharename);
            rs = stmt.executeQuery();
            return rs;
            // Now do something with the ResultSet ....
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("Fehler aus getTableRowByID()");
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
        }
        return rs;

    }

    public void createDepot(String depot_name) {

        this.connect();
// assume that conn is an already created JDBC connection (see previous examples)
        Statement stmt = null;

        try {
            stmt = conn.createStatement();
            stmt.executeUpdate("CREATE TABLE `deponiedb`.`" + depot_name + "` (\n"
                    + "  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,\n"
                    + "  `share` varchar(11) NOT NULL DEFAULT '',\n"
                    + "  `buyDate` date DEFAULT NULL,\n"
                    + "  `sharePrice` double(11,4) DEFAULT NULL,\n"
                    + "  `buyNr` int(7) UNSIGNED DEFAULT NULL,\n"
                    + "  `costs` double(11,2) UNSIGNED DEFAULT NULL,\n"
                    + "  `sellDate` date DEFAULT NULL,\n"
                    + "  `sellNr` int(7) UNSIGNED DEFAULT NULL,\n"
                    + "  `shareProceed` double(11,4) UNSIGNED DEFAULT NULL,\n"
                    + "  `proceeds` double(11,2)  UNSIGNED DEFAULT NULL,\n"
                    + "  `profit` double(11,2)  UNSIGNED DEFAULT NULL,\n"
                    + "  `loss` double(11,2)  DEFAULT NULL,\n"
                    + "  `inventory` int(7) UNSIGNED DEFAULT NULL,\n"
                    + "  `bookValue` double(11,2) DEFAULT NULL,\n"
                    + "  `changedBy` varchar(32) NOT NULL DEFAULT '',\n"
                    + "  `isCash` boolean NOT NULL DEFAULT '0',\n"
                    + "  PRIMARY KEY (`id`),\n"
                    + "  UNIQUE KEY `id` (`id`),\n"
                    + "  KEY `isCash` (`isCash`),\n"
                    + "  KEY `share` (`share`)"
                    + ") ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT '" + this.user + "';");
            JOptionPane.showMessageDialog(null, "Depot " + depot_name
                    + " has been created.", "Depot created!", JOptionPane.INFORMATION_MESSAGE);

            // Handle SQL errors
        } catch (SQLException ex) {
            // handle any errors
            System.err.println("SQLException: " + ex.getMessage());
            System.err.println("SQLState: " + ex.getSQLState());
            System.err.println("VendorError: " + ex.getErrorCode());
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
        }

    }

    public void changePassword(String pwOld, String pwNew) {

        String sql;
        Statement stmt = null;
        //sql = "SET PASSWORD FOR '" + user + "'@'localhost' = PASSWORD('" + pwNew + "');";
        sql = "SET PASSWORD = PASSWORD('" + pwNew + "');";

        try {
            conn = DriverManager.getConnection(url, user, pwOld);
            stmt = conn.createStatement();
            stmt.executeQuery(sql);
            stmt.close();
            JOptionPane.showMessageDialog(null, "Password has been changed!\nProgramm will shut down!\nPlease restart manually.", "Password changed", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        } catch (SQLException ex) {
            // handle any errors
            if (ex.getErrorCode() != 1133) {
                System.err.println("SQLException: " + ex.getMessage());
                System.err.println("SQLState: " + ex.getSQLState());
                System.err.println("VendorError: " + ex.getErrorCode());
                System.err.println("Class: user.Database.changePassword()");
                JOptionPane.showMessageDialog(null, "Database error: " + ex.getMessage() + "\nClass: user.Database.changePassword()", "Error", JOptionPane.ERROR_MESSAGE);
            } else {

                JOptionPane.showMessageDialog(null, "Password has been changed!\nProgramm will shut down!\nPlease restart manually.", "Password changed!", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }
        } catch (Exception ex) {
            // handle any errors
            System.err.println("Error in Class: user.Database.changePassword()");
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage() + "\nClass: user.Database.changePassword()", "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            disconnect();
        }
    }

    public void changeUserPassword(String User, String pwd) {

        String sql;
        Statement stmt = null;
        sql = "SET PASSWORD FOR '" + User + "' = PASSWORD('" + pwd + "');";
        //sql = "UPDATE mysql.USER SET Password=PASSWORD('new-password-here') WHERE USER='user-name-here';";

        try {
            conn = DriverManager.getConnection(url, user, pw);
            stmt = conn.createStatement();
            stmt.executeQuery(sql);
            stmt.close();
            JOptionPane.showMessageDialog(null, "Password has been changed!", "Password changed", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            // handle any errors
            if (ex.getErrorCode() != 1133) {
                System.err.println("SQLException: " + ex.getMessage());
                System.err.println("SQLState: " + ex.getSQLState());
                System.err.println("VendorError: " + ex.getErrorCode());
                System.err.println("Class: Database.changeUserPassword()");
                JOptionPane.showMessageDialog(null, "Database error: " + ex.getMessage() + "\nClass: Database.changeUserPassword()", "Error", JOptionPane.ERROR_MESSAGE);
            } else {

                JOptionPane.showMessageDialog(null, "Password has been changed!\nSQL: (" + ex.getMessage() + ")", "Password changed!", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
            // handle any errors
            System.err.println("Error in Class: Database.changeUserPassword()");
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage() + "\nClass: Database.changeUserPassword()", "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            disconnect();
        }
    }

    public void addUser(String User, String pwd) {

        Connection currentconnection = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;

        String sql1, sql2;
        Statement stmt = null;
        //sql0 = "DELETE FROM mysql.db where user='username';";
        sql1 = "CREATE USER ?@'%' IDENTIFIED BY ?;\n";
        sql2 = "GRANT SELECT, INSERT, UPDATE , "
                + "CREATE ON * . * TO ?@'%' IDENTIFIED BY ?"
                + " WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;";

        try {
            this.connect();
            currentconnection = this.getConnection();
            // Disable autocommit to be able to create transaction
            currentconnection.setAutoCommit(false);

            pstmt1 = currentconnection.prepareStatement(sql1);
            pstmt1.setString(1, User);
            pstmt1.setString(2, pwd);
            pstmt1.execute();

            pstmt1 = currentconnection.prepareStatement(sql2);
            pstmt1.setString(1, User);
            pstmt1.setString(2, pwd);
            pstmt1.execute();

            currentconnection.commit();
            // enable auto commit again
            currentconnection.setAutoCommit(true);

            this.disconnect();
            JOptionPane.showMessageDialog(null, "User has been added!", "User added", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            // handle any errors
            if (ex.getErrorCode() != 1396) {
                System.err.println("SQLException: " + ex.getMessage());
                System.err.println("SQLState: " + ex.getSQLState());
                System.err.println("VendorError: " + ex.getErrorCode());
                System.err.println("Class: user.Database.addUser()");
                JOptionPane.showMessageDialog(null, "Database error: " + ex.getMessage() + "\n"
                        + "Class: Database.addUser()", "Error", JOptionPane.ERROR_MESSAGE);
            } else {

                JOptionPane.showMessageDialog(null, "User already exists!", "User exists!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            // handle any errors
            System.err.println("Error in Class: Database.addUser()");
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage() + "\nClass: Database.addUser()", "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            disconnect();
        }
    }

    public void deleteUser(String User) {

        Connection currentconnection = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;

        String sql1, sql2;
        Statement stmt = null;
        //sql0 = "DELETE FROM mysql.db where user='username';";
        sql1 = "DROP USER ?@'%';";
        sql2 = "FLUSH PRIVILEGES;";

        try {
            this.connect();
            currentconnection = this.getConnection();
            // Disable autocommit to be able to create transaction
            currentconnection.setAutoCommit(false);

            pstmt1 = currentconnection.prepareStatement(sql1);
            pstmt1.setString(1, User);
            pstmt1.execute();

            pstmt1 = currentconnection.prepareStatement(sql2);
            pstmt1.execute();

            currentconnection.commit();
            // enable auto commit again
            currentconnection.setAutoCommit(true);

            this.disconnect();
            JOptionPane.showMessageDialog(null, "User has been deleted!", "User deleted", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            // handle any errors
            if (ex.getErrorCode() != 1396) {
                System.err.println("SQLException: " + ex.getMessage());
                System.err.println("SQLState: " + ex.getSQLState());
                System.err.println("VendorError: " + ex.getErrorCode());
                System.err.println("Class: user.Database.deleteUser()");
                JOptionPane.showMessageDialog(null, "Database error: " + ex.getMessage() + "\n"
                        + "Class: Database.deleteUser()", "Error", JOptionPane.ERROR_MESSAGE);
            } else {

                JOptionPane.showMessageDialog(null, "User does not exists!", "ERROR!", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            // handle any errors
            System.err.println("Error in Class: user.Database.changePassword()");
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage() + "\nClass: Database.deleteUser()", "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            disconnect();
        }
    }

    public void BackupDB() {
        try {

            /*NOTE: Getting path to the Jar file being executed*/
            CodeSource codeSource = Database.class.getProtectionDomain().getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI().getPath());
            String jarDir = jarFile.getParentFile().getPath();

            /*NOTE: Creating Path Constraints for folder saving*/
            /*NOTE: Here the backup folder is created for saving inside it*/
            String folderPath = jarDir + "\\backup";
            //String folderPath = jarDir;

            /*NOTE: Creating Folder if it does not exist*/
            File f1 = new File(folderPath);
            f1.mkdir();

            /*NOTE: Creating Path Constraints for backup saving*/
            /*NOTE: Here the backup is saved in a folder called backup with the name backup.sql*/
            //String savePath = "\"" + jarDir + "\\backup\\" + "backup.sql\"";
            String savePath = jarDir + "/backup.sql";

            /*NOTE: Used to create a cmd command*/
            //String executeCmd = "C:\\xamppnew\\mysql\\bin\\mysqldump " + db + " -u" + user + " -p" + pw + " -r " + savePath;
            String executeCmd = "mysqldump " + db + " -u" + user + " -p" + pw + " -r " + savePath;

            /*NOTE: Executing the command here*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();

            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                System.out.println("Backup Complete");
                JOptionPane.showMessageDialog(null, "Backup Complete: " + jarDir);
            } else {
                System.out.println("Backup Failure");
                JOptionPane.showMessageDialog(null, "Backup Failure. Error " + processComplete,
                        "Backup Error", JOptionPane.ERROR_MESSAGE);

            }

        } catch (URISyntaxException | IOException | InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Error at Backuprestore" + ex.getMessage(),
                    "Backup Error", JOptionPane.ERROR_MESSAGE);
            //} catch (Exception ex) {
            //    JOptionPane.showMessageDialog(null, "Error at Backuprestore" + ex.getMessage());
        }
    }
}
