/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forms;

import java.awt.Component;
import common.Database;
import java.awt.Toolkit;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jona
 */
public class DepotManagerForm extends javax.swing.JFrame {
    
    // Data Classes
    private final Database userDB;
    private final String currentDepot;
    
    // Frame Classes
    private DepotBuyShares frameBuyShare = null;
    private DepotSellShares frameSellShare = null;
    private DepotTransferShare frameTransferShare = null;
    private StatementSchedule frameStatementSchedule =null;
    private DepotCash frameCash = null;
    
    


    /**
     * Creates new form DepotManagerForm
     *
     * @param userDBReference
     * @param depotname
     * @throws java.sql.SQLException
     */
    public DepotManagerForm(Database userDBReference, String depotname) throws SQLException {
        initComponents();
        this.userDB = userDBReference;
        this.currentDepot = depotname;
        // only enable delete rows for admin
        if (!this.userDB.isAdmin()){
            bDeleteShares.setVisible(false);
        }
        this.setTitle("Depot No.:  " + depotname);
        Toolkit fullscreentk = Toolkit.getDefaultToolkit();
        int xsize = (int) fullscreentk.getScreenSize().getWidth();
        int ysize = (int) fullscreentk.getScreenSize().getHeight();
        this.setSize(xsize,ysize);
        
        // Set up Jtable GUI and fetch data from server
        initSharesJavaTable();
        refreshSharesJavaTable();
    }
     // Getter Methods

    public JTable getTblDepot() {
        return tblDepot;
    }
    
    // sets up correct GUI for Java Table. Is called in constructor
    private void initSharesJavaTable() {
        // set up JTable formatter for precicion value and right allignment
        Float2DecimalFormatRenderer float2prececionrenderer = new Float2DecimalFormatRenderer();
        Float4DecimalFormatRenderer float4prececionrenderer = new Float4DecimalFormatRenderer();
        DefaultTableCellRenderer rightAllignmentRenderer = new DefaultTableCellRenderer();
        
        // sez the horizontal allignment for colums
        rightAllignmentRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        float2prececionrenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        float4prececionrenderer.setHorizontalAlignment(SwingConstants.RIGHT);
        // only one row at a time can be selected
        tblDepot.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // don't allow drag and drop of single colums
        tblDepot.getTableHeader().setReorderingAllowed(false);
        
        // formating the coloums with appropriate renders
        tblDepot.getColumnModel().getColumn(1).setCellRenderer(
                rightAllignmentRenderer);
        tblDepot.getColumnModel().getColumn(2).setCellRenderer(
                rightAllignmentRenderer);
        tblDepot.getColumnModel().getColumn(4).setCellRenderer(
                float4prececionrenderer);
        tblDepot.getColumnModel().getColumn(5).setCellRenderer(
                float2prececionrenderer);
        tblDepot.getColumnModel().getColumn(6).setCellRenderer(
                rightAllignmentRenderer);
        tblDepot.getColumnModel().getColumn(8).setCellRenderer(
                float4prececionrenderer);
        tblDepot.getColumnModel().getColumn(9).setCellRenderer(
                float2prececionrenderer);
        tblDepot.getColumnModel().getColumn(10).setCellRenderer(
                float2prececionrenderer);
        tblDepot.getColumnModel().getColumn(11).setCellRenderer(
                float2prececionrenderer);
        tblDepot.getColumnModel().getColumn(13).setCellRenderer(
                float2prececionrenderer);
        tblDepot.getColumnModel().getColumn(14).setCellRenderer(
                rightAllignmentRenderer);
        
        // removes id coloumn from GUI, but still keeps it in getTblModel
        tblDepot.removeColumn(tblDepot.getColumnModel().getColumn(0));
        // make changedby coloumn only visible for admin
        if(!userDB.isAdmin()) {
            tblDepot.removeColumn(tblDepot.getColumnModel().getColumn(13));
        }
        // this is the override method to handle selected items in jTable
        tblDepot.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                int rowselected = tblDepot.getSelectedRow();
                if (rowselected > -1) {
                    int inventory = Integer.parseInt(tblDepot.getModel().getValueAt(rowselected, 12).toString());
                    bDeleteShares.setEnabled(true);
                    if (inventory > 0) {
                        bSellShares.setEnabled(true);
                    } else {
                        bSellShares.setEnabled(false);
                    }
                } else {
                    bSellShares.setEnabled(false);
                    bDeleteShares.setEnabled(false);
                }
            }
        });
    }
    
    
    public final void refreshSharesJavaTable() {

        DefaultTableModel model = (DefaultTableModel) tblDepot.getModel();
        int numRows = model.getRowCount();
        if (numRows != 0) {
            for (int i = numRows - 1; i >= 0; i--) {
                model.removeRow(i);
            }
        }
            /* SQL table entrys
              `id`(1)
              `share`(2) 
              `buyDate` (3)
              `sharePrice (4)
              `buyNr` (4)
              `costs`   (6)
              `sellDate` (7)
              `sellNr` (8)
              `shareProceed` (9)
               `proceeds` (10)
               `profit` (11)
               `loss`  (12)
               `inventory` (13)
              `bookValue` (14)
               `changedBy` (15) */
        ResultSet depotData = userDB.getSortedDepotFromServer(currentDepot,false);
        
        int count = 0;
        SimpleDateFormat ft =  new SimpleDateFormat ("dd.MM.yyyy");
        try {
            // while tablerows in sql table
            while (depotData.next()) {
  
                
                int id = depotData.getInt("id");
                String share = depotData.getString("share");
                Date buyDate = depotData.getDate("buyDate");
                double sharePrice = depotData.getDouble("sharePrice");
                int numberBuy = depotData.getInt("buyNr");
                double costs = depotData.getDouble("costs");
                Date sellDate = depotData.getDate("sellDate");
                int numberSell = depotData.getInt("sellNr");
                double shareproceed = depotData.getDouble("shareProceed");
                double proceeds = depotData.getDouble("proceeds");
                double profit = depotData.getDouble("profit");
                double loss = depotData.getDouble("loss");
                int inventory = depotData.getInt("inventory");
                double bookValue = depotData.getDouble("bookValue");
                String changedby = depotData.getString("changedBy");
               
                // check the sql dates and do according refractoring for java table
                if ((sellDate != null) && (buyDate != null)){
                    Object[] row = {id, share,ft.format(buyDate), numberBuy,sharePrice, costs, ft.format(sellDate), numberSell,
                                    shareproceed, proceeds, profit,loss, inventory, bookValue,changedby};
                    model.addRow(row);
                }
                else if (buyDate != null){
                    Object[] row = {id, share, ft.format(buyDate), numberBuy,sharePrice, costs, sellDate, numberSell, 
                                    shareproceed, proceeds, profit,loss, inventory, bookValue, changedby};
                    model.addRow(row);
                }
                else if (sellDate != null){
                    Object[] row = {id, share,buyDate, numberBuy,sharePrice, costs, ft.format(sellDate), numberSell, 
                                    shareproceed , proceeds, profit,loss, inventory, bookValue,changedby};
                    model.addRow(row);
                }
                else {
                    Object[] row = {id, share, buyDate, numberBuy,sharePrice, costs, sellDate, numberSell, 
                                    shareproceed,proceeds, profit,loss, inventory, bookValue,changedby};
                    model.addRow(row);
                }
                ++count;
            }
            this.userDB.disconnect();
            System.out.println(count + " shares were retrieved");
        } catch (SQLException sqlex) {
            System.err.println("Error aus refreshJavaTable");
            System.err.println("SQLException: " + sqlex.getMessage());
            System.err.println("SQLState: " + sqlex.getSQLState());
            System.err.println("VendorError: " + sqlex.getErrorCode());
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bAddShares = new javax.swing.JButton();
        bSellShares = new javax.swing.JButton();
        bDeleteShares = new javax.swing.JButton();
        bTransferShare = new javax.swing.JButton();
        bCashDividend = new javax.swing.JButton();
        bStatement = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDepot = new javax.swing.JTable();
        bClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(new java.awt.Point(40, 40));
        setMinimumSize(new java.awt.Dimension(1180, 300));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        bAddShares.setText("Buy Shares");
        bAddShares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAddSharesActionPerformed(evt);
            }
        });

        bSellShares.setText("Sell Shares");
        bSellShares.setEnabled(false);
        bSellShares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSellSharesActionPerformed(evt);
            }
        });

        bDeleteShares.setText("Delete Shares");
        bDeleteShares.setEnabled(false);
        bDeleteShares.setMaximumSize(new java.awt.Dimension(114, 25));
        bDeleteShares.setMinimumSize(new java.awt.Dimension(114, 25));
        bDeleteShares.setPreferredSize(new java.awt.Dimension(114, 25));
        bDeleteShares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteSharesActionPerformed(evt);
            }
        });

        bTransferShare.setText("Transfer Shares");
        bTransferShare.setToolTipText("");
        bTransferShare.setMaximumSize(new java.awt.Dimension(87, 23));
        bTransferShare.setMinimumSize(new java.awt.Dimension(87, 23));
        bTransferShare.setPreferredSize(new java.awt.Dimension(87, 23));
        bTransferShare.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bTransferShareActionPerformed(evt);
            }
        });
        bTransferShare.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bTransferShareKeyPressed(evt);
            }
        });

        bCashDividend.setText("Cash / Dividend");
        bCashDividend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCashDividendActionPerformed(evt);
            }
        });

        bStatement.setText("Statement / Fee Schedule");
        bStatement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bStatementActionPerformed(evt);
            }
        });

        tblDepot.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "share", "buy date", "number", "share price", "costs", "sell date", "number", "share proceed", "proceeds", "gross profit", "loss", "inventory", "book value", "changed by"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Integer.class, java.lang.Double.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDepot.setEditingRow(1);
        tblDepot.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblDepot);
        if (tblDepot.getColumnModel().getColumnCount() > 0) {
            tblDepot.getColumnModel().getColumn(1).setPreferredWidth(140);
            tblDepot.getColumnModel().getColumn(2).setPreferredWidth(95);
            tblDepot.getColumnModel().getColumn(3).setMinWidth(50);
            tblDepot.getColumnModel().getColumn(3).setPreferredWidth(85);
            tblDepot.getColumnModel().getColumn(3).setMaxWidth(110);
            tblDepot.getColumnModel().getColumn(5).setMinWidth(90);
            tblDepot.getColumnModel().getColumn(5).setPreferredWidth(130);
            tblDepot.getColumnModel().getColumn(5).setMaxWidth(130);
            tblDepot.getColumnModel().getColumn(6).setPreferredWidth(95);
            tblDepot.getColumnModel().getColumn(7).setMinWidth(50);
            tblDepot.getColumnModel().getColumn(7).setPreferredWidth(85);
            tblDepot.getColumnModel().getColumn(7).setMaxWidth(110);
            tblDepot.getColumnModel().getColumn(9).setMinWidth(90);
            tblDepot.getColumnModel().getColumn(9).setPreferredWidth(130);
            tblDepot.getColumnModel().getColumn(9).setMaxWidth(150);
            tblDepot.getColumnModel().getColumn(10).setMinWidth(90);
            tblDepot.getColumnModel().getColumn(10).setPreferredWidth(130);
            tblDepot.getColumnModel().getColumn(10).setMaxWidth(150);
            tblDepot.getColumnModel().getColumn(12).setMinWidth(50);
            tblDepot.getColumnModel().getColumn(12).setPreferredWidth(85);
            tblDepot.getColumnModel().getColumn(12).setMaxWidth(110);
            tblDepot.getColumnModel().getColumn(13).setMinWidth(80);
            tblDepot.getColumnModel().getColumn(13).setPreferredWidth(85);
            tblDepot.getColumnModel().getColumn(13).setMaxWidth(85);
            tblDepot.getColumnModel().getColumn(14).setResizable(false);
            tblDepot.getColumnModel().getColumn(14).setPreferredWidth(95);
        }

        bClose.setText("Close Depot");
        bClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bAddShares, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bSellShares, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bDeleteShares, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bTransferShare, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                        .addComponent(bCashDividend, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bStatement)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bClose, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bAddShares, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bSellShares, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bDeleteShares, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bTransferShare, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bCashDividend, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bStatement, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bClose, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
                .addContainerGap())
        );

        setBounds(0, 0, 1289, 715);
    }// </editor-fold>//GEN-END:initComponents

   
/////////////////////    
/// Event-Handlers///
///////////////////// 
    
    private void bSellSharesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSellSharesActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            if (frameSellShare != null) {
                frameSellShare.dispose();

            }
            frameSellShare = new DepotSellShares(userDB, this,currentDepot);
            frameSellShare.setVisible(true);
        });
    }//GEN-LAST:event_bSellSharesActionPerformed

    private void bAddSharesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAddSharesActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            if (frameBuyShare != null) {
                frameBuyShare.dispose();

            }
            frameBuyShare = new DepotBuyShares(userDB, this, currentDepot);
            frameBuyShare.setVisible(true);
        });
    }//GEN-LAST:event_bAddSharesActionPerformed

    private void bCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCloseActionPerformed
        // TODO add your handling code here:
        // JOptionPane.showMessageDialog(this, "This function is not implemented yet!", "Not impemented", JOptionPane.ERROR_MESSAGE);
       
        DepotForm newDepotForm = new DepotForm(this.userDB);
        newDepotForm.setVisible(true);
         if (this.frameStatementSchedule != null) {
            frameStatementSchedule.dispose();
        }
        if (this.frameTransferShare != null) {
            frameTransferShare.dispose();
        } 
        if (this.frameSellShare != null) {
            frameSellShare.dispose();
        } 
        if (this.frameBuyShare != null) {
            frameBuyShare.dispose();
        }
        if (this.frameCash != null) {
            frameCash.dispose();
        }
        this.dispose();
    }//GEN-LAST:event_bCloseActionPerformed
   
    private void bStatementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bStatementActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            if (frameStatementSchedule != null) frameStatementSchedule.dispose();
            frameStatementSchedule = new StatementSchedule(userDB,currentDepot);
            frameStatementSchedule.setVisible(true);
        });
    }//GEN-LAST:event_bStatementActionPerformed

    private void bTransferShareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bTransferShareActionPerformed
        java.awt.EventQueue.invokeLater(() -> {
            if (frameTransferShare != null) {
                frameTransferShare.dispose();

            }
            frameTransferShare = new DepotTransferShare(this.userDB, this,currentDepot);
            frameTransferShare.setVisible(true);
        });
    }//GEN-LAST:event_bTransferShareActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        DepotForm newDepotForm = new DepotForm(this.userDB);
        newDepotForm.setVisible(true);
        if (this.frameStatementSchedule != null) {
            frameStatementSchedule.dispose();
        }
        if (this.frameTransferShare != null) {
            frameTransferShare.dispose();
        } 
        if (this.frameSellShare != null) {
            frameSellShare.dispose();
        } 
        if (this.frameBuyShare != null) {
            frameBuyShare.dispose();
        }
        if (this.frameCash != null) {
            frameCash.dispose();
        }
        
    }//GEN-LAST:event_formWindowClosing

    private void bTransferShareKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bTransferShareKeyPressed
        if (evt.getKeyCode()==java.awt.event.KeyEvent.VK_ENTER){
            java.awt.event.ActionEvent Aevt =  null;
            bTransferShareActionPerformed(Aevt);
            
        }
    }//GEN-LAST:event_bTransferShareKeyPressed

    private void bDeleteSharesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteSharesActionPerformed
        int rowselected = tblDepot.getSelectedRow();       
        String sharename = tblDepot.getModel().getValueAt(rowselected, 1).toString();
        int shareID = Integer.parseInt(tblDepot.getModel().getValueAt(tblDepot.getSelectedRow(),0).toString());
        int dialogResult = JOptionPane.showOptionDialog(this, "Are you sure you want "
                                         + "to delete the share " + sharename + " ?",
                                        "Delete Share", JOptionPane.DEFAULT_OPTION,
                                        JOptionPane.WARNING_MESSAGE, null, 
                                        new String[] {"No", "Yes"},"No");
        
        if(dialogResult == 1) {
            try {
                String sqlstring1 = "DELETE FROM " + userDB.getDataBase() + ".`" + currentDepot  + 
                "` WHERE id=?";
                userDB.connect();
                PreparedStatement pstmt1 = userDB.getConnection().prepareStatement(sqlstring1);
                pstmt1.setInt(1, shareID);
                pstmt1.executeUpdate();
                refreshSharesJavaTable();
                JOptionPane.showMessageDialog(null, "Share has been "
                         + "succesfully deleted", "Share deleted!", 
                     JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException sqlex) {
                // handle any errors
                System.err.println("SQLException: " + sqlex.getMessage());
                System.err.println("SQLState: " + sqlex.getSQLState());
                System.err.println("VendorError: " + sqlex.getErrorCode());
                JOptionPane.showMessageDialog(this, sqlex.getMessage(), 
                    "Data Error", JOptionPane.ERROR_MESSAGE);
            } finally {
                userDB.disconnect();
            }  
        } 
    }//GEN-LAST:event_bDeleteSharesActionPerformed

    private void bCashDividendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCashDividendActionPerformed
        // TODO add your handling code here:
        java.awt.EventQueue.invokeLater(() -> {
            if (frameCash != null) {
                frameCash.dispose();

            }
            frameCash = new DepotCash(userDB,currentDepot);
            frameCash.setVisible(true);
        });
    }//GEN-LAST:event_bCashDividendActionPerformed

    // Set the right format for given double coloums in table, so the are
    // displayed with two point precicion
    static class Float2DecimalFormatRenderer extends DefaultTableCellRenderer {

        private static final DecimalFormat formatter = new DecimalFormat("0.00 £");

        @Override
        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {

         // First format the cell value as required
            value = formatter.format((Number) value);

            // And pass it on to parent class
            return super.getTableCellRendererComponent(
                    table, value, isSelected, hasFocus, row, column);
        }
    }
    
    // Set the right format for given double coloums in table, so the are
    // displayed with four point precicion    
    static class  Float4DecimalFormatRenderer extends DefaultTableCellRenderer {
       private static final DecimalFormat formatter = new DecimalFormat("0.0000 £");

        @Override
        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {

         // First format the cell value as required
            value = formatter.format((Number) value);

            // And pass it on to parent class
            return super.getTableCellRendererComponent(
                    table, value, isSelected, hasFocus, row, column);
        }
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAddShares;
    private javax.swing.JButton bCashDividend;
    private javax.swing.JButton bClose;
    private javax.swing.JButton bDeleteShares;
    private javax.swing.JButton bSellShares;
    private javax.swing.JButton bStatement;
    private javax.swing.JButton bTransferShare;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDepot;
    // End of variables declaration//GEN-END:variables
}
